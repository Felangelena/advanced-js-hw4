"use strict";

const listOfEpisodes = document.querySelector('#listOfEpisodes');

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(data => {
    data.forEach((film) => {
      let episode = document.createElement('li');
      episode.innerHTML = `<b>Episode ${film.episodeId} - ${film.name}</b> </br> <b>Opening crawl:</b> ${film.openingCrawl}`;
      listOfEpisodes.append(episode);
      let listOfCharacters = document.createElement('ul');
      listOfCharacters.textContent = 'Characters:';
      episode.append(listOfCharacters);

      film.characters.forEach((character) => {
        fetch(character)
          .then(res => res.json())
          .then(person => {
            let personName = document.createElement('li');
            personName.textContent = person.name;
            listOfCharacters.append(personName);
      });
    });
  });
});